# Arrietty

`arrietty` is a fork of `Archive.org-Downloader` by MiniGlome,
a Python3 script to borrow books from the Internet Archive.

See <https://github.com/MiniGlome/Archive.org-Downloader>
for the original version.

See <https://code.mathr.co.uk/arrietty>
for this version.

## About

There are many great books available on <https://openlibrary.org>
and <https://archive.org>, however, for some items
you can only borrow them for 1 hour
to 14 days and you don't have the option to download it as a PDF to read
it offline.  MiniGlome created this program to solve these problems and
retrieve the original book in PDF format.

Of course, the download takes a few minutes depending on the number of
pages and the quality of the images you have selected. You must also
create an account on <https://archive.org> for the script to work with
items that need to be borrowed.

## Prerequisites

### Debian

```sh
sudo apt install git make dos2unix pandoc python3 \
  python3-requests python3-tqdm python3-img2pdf python3-xdg
```

### Other OS

You need `python3` to run the script.
You can download it here: <https://python.org>.

You need `git` to get the script.
You can download it here: <https://git-scm.com>.

You need the Python modules `requests`, `tqdm`, `xdg` and `img2pdf`.
You can install them with this command:

```sh
pip install -r requirements.txt
```

You need Pandoc to generate the `man` page.
You can download it here: <https://pandoc.org>.

You need `dos2unix` to build the script.

## Download

To get the script:

```sh
git clone https://code.mathr.co.uk/arrietty.git
```

## Build

To build the script:

```sh
cd arrietty
make
```

## Install

Installation is optional, you can use the script without installing:

```sh
/path/to/arrietty/arrietty --help
```

To install to `${HOME}/.local`:

```sh
cd arrietty
make
make install
```

To install to `/usr/local`:

```sh
cd arrietty
make
sudo make install DESTDIR="/" prefix="usr/local"
```

## Usage

See the user manual ([`arrietty.1.md`](arrietty.1.md)),
or after installation:

```sh
man arrietty
```

## Compatibility

There are many differences from MiniGlome's original version.

### Arguments From Files

The `-u` `--url` `-f` `--file` options have been removed.
Now just put the URLs without `-u`,
and use `@myfile` instead of `-f myfile`.

You can also put any other arguments in the file,
useful for storing credentials.

### XDG Configuration File

A configuration file is prepended to the command line arguments,
if it exists.
The specific location depends on the
`$XDG_CONFIG_HOME` environment variable,
and is printed at program start.

### Keyboard Interrupt

Much better behaviour when interrupted with Ctrl-C.
However,it still waits for all current downloads to finish,
so it is recommended to reduce the `-t` `--threads` count.

### Resume Download

Download folders and PDFs are named after the book identifier.
When downloading, if the PDF exists,
the download is skipped entirely.
When downloading, if some JPGs already exist,
those pages are not downloaded again.

### Zero-Pad Page Numbers

Page numbers are padded with leading zero,
so that lexical ordering is equal to numerical ordering,
reducing risk of problems with pages out of order in other software.

### URLs Can Be Book IDs

The prefix `https://archive.org/details/` is added automatically
to any URL arguments that don't contain a `/`.
URLs with the prefix work as before.

### PDF Metadata

Author, title, and year of publication information
are stored in the PDF (if available).
The book URL is included in the PDF keywords.

(This change has been included into recent MiniGlome versions.)

### Some Books Can Be Downloaded Without Login / Borrow

If the book does not need to be borrowed for download, then you don't
need to provide an email address or password.

Adapted from
<https://github.com/MiniGlome/Archive.org-Downloader/pull/40>

### Documentation

User manual as `man` page, and
rewritten project documentation (this file).

### Installer

A `make`-based build system installs the script as `arrietty`.

## Donate

If you want to support MiniGlome's work, see
<https://github.com/MiniGlome/Archive.org-Downloader#donation>.

If you want to support the Internet Archive, see
<https://archive.org/donate>.
