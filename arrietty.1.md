---
title: ARRIETTY
section: 1
header: User Commands
footer: 0.1
author: MiniGlome, Claude Heiland-Allen, and others
date: 2024-04-20
---

NAME
====

arrietty - borrow books from the Internet Archive

SYNOPSIS
========

**arrietty** \[*option*...\] *URL* ...

DESCRIPTION
===========

**arrietty** is a tool to borrow books from the Internet Archive, using
your Internet Archive account if necessary.

Each *URL* argument given to **arrietty** contains the identifier of a
book on the Internet Archive like `https://archive.org/details/`*book*.
For each *URL*, the book is borrowed, the page images are downloaded,
and converted to a PDF file.

SECURITY
========

Passing credentials on the command line is insecure on some operating
systems.  Put credentials in a file with restricted permissions, and
use the **@file** functionality.

OPTIONS
=======

*URL* ...

:   The URLs of the books download, of the form
    `https://archive.org/details/`*book*, either the full URL with the
    prefix, or just the *book* identifier.  The output will be stored in
    the file *book*`.pdf`, or if **\--jpg** is specified, in a directory
    *book*`/`.

**-h**, **\--help**

:   Print a help message and exit.

**-e** *email*, **\--email** *email*

:   The email address associated with your Internet Archive account.
    This option is required if the book needs to be borrowed.

**-p** *password*, **\--password** *password*

:   The password associated with your Internet Archive account.
    This option is required if the book needs to be borrowed.

**-s** *skip_login*, **\--skip_login** *skip_login*

:   Use this if you want to download only books
    which do not need to be borrowed.
    Allows email and password to be omitted.

**-l** *loan_always*, **\--loan_always** *load_always*

:   Always try to loan all added books.
    Can improve speed if you're planning to download only books
    which need to be loaned.

**-r** *resolution*, **\--resolution** *resolution*

:   Image quality (*0* is best quality with largest size, *10* is worst
    quality with smallest size).  The default is *3*.

**-t** *threads*, **\--threads** *threads*

:   The number of images to download in parallel.  The default is *50*.

**-d** *directory*, **\--directory** *directory*

:   Output directory for all downloads.  The default is the current
    working directory.

**-j**, **--jpg**

:   Output a directory of JPG images, rather than a PDF file.

**-m**, **--meta**

:   Output the metadata of the book to a JSON file.

You can use **@myfile** to read arguments from the file **myfile**, one
per line.  For example, to store credentials:

```
--email
user@example.com
--password
letmein
```

or to batch download a list of books:

```
borrowers00nortrich
borrowersafield00nortrich
borrowersafloat00nortrich
borrowersaloft0000nort
borrowersavenged00mary
```

FILES
=====

**${HOME}/.config/arrietty.conf**

:   Default arguments, one per line.  A good place to store credentials,
    but set the file permissions so that other users can't read them.

EXIT STATUS
===========

**0**

:   Successful program execution.

**1**

:   An error occured or **arrietty** was interrupted.

EXAMPLES
========

To download a book in best quality:

```
arrietty -e user@example.com -p letmein -r 0 borrowers00nortrich
```

To download a book in default quality with credentials in a file:

```
arrietty @credentials.txt borrowers00nortrich
```

To download a list of books stored in the file **books.txt**, with
credentials (and potentially other options) in
**${HOME}/.config/arrietty.conf**:

```
arrietty @books.txt
```

HISTORY
=======

2021,2022,2024 -- original version by MiniGlome and others
`https://github.com/MiniGlome/Archive.org-Downloader`

2022,2024 -- improvements by Claude Heiland-Allen and others
`https://code.mathr.co.uk/arrietty`
