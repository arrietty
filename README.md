# IMPORTANT

This file (`README.md`) is obsolete in this fork.

It is only still here to ease merging of upstream changes.

Please see the project documentation in
[`arrietty.md`](arrietty.md)
and the user manual in
[`arrietty.1.md`](arrietty.1.md).

---

![made-with-python](https://img.shields.io/badge/Made%20with-Python3-brightgreen)

<!-- LOGO -->
<br />
<p align="center">
  <img src="https://user-images.githubusercontent.com/54740007/108192715-e5958c80-7114-11eb-8240-e884895bb45f.png" alt="Logo" width="80" height="80">

  <h3 align="center">Archive.org-Downloader</h3>

  <p align="center">
    Python3 script to download archive.org books in PDF format !
    <br />
    </p>
</p>


## About The Project

There are many great books available on https://openlibrary.org/ and https://archive.org/, however, many of them need to be borrowed and you can only borrow them for 1 hour to 14 days and you don't have the option to download it as a PDF to read it offline or share it with your friends. I created this program to solve this problem and retrieve the original book in pdf format for FREE !

Of course, the download takes a few minutes depending on the number of pages and the quality of the images you have selected. You must also create an account on https://archive.org/ for the script to work.


## Getting Started
To get started you need to have python3 installed. If it is not the case you can download it here : https://www.python.org/downloads/

### Installation
Make sure you've already git installed. Then you can run the following commands to get the scripts on your computer:
   ```sh
   git clone https://github.com/MiniGlome/Archive.org-Downloader.git
   cd Archive.org-Downloader
   ```
The script requires the modules `requests`, `tqdm`, `xdg` and `img2pdf`, you can install them all at once with this command:
```sh
pip install -r requirements.txt
```
   
## Usage
```
usage: archive-org-downloader.py [-h] [-e EMAIL] [-p PASSWORD] [-s SKIP_LOGIN]
                                 [-d DIR] [-r RESOLUTION]
                                 [-t THREADS] [-j] [-l LOAN_ALWAYS]
                                 URL ...

positional arguments:
  URL                   Link to the book (https://archive.org/details/XXXX).
                        You can use this argument several times to download multiple books

options:
  -h, --help            show this help message and exit
  -e EMAIL, --email EMAIL
                        Your archive.org email
  -p PASSWORD, --password PASSWORD
                        Your archive.org password
  -s SKIP_LOGIN, --skip_login SKIP_LOGIN
                        Use this if you want to download books which do not
                        need to be borrowed.
  -d DIR, --dir DIR     Output directory
  -r RESOLUTION, --resolution RESOLUTION
                        Image resolution (10 to 0, 0 is the highest), [default
                        3]
  -t THREADS, --threads THREADS
                        Maximum number of threads, [default 50]
  -j, --jpg             Output to individual JPG's rather than a PDF
  -l LOAN_ALWAYS, --loan_always LOAN_ALWAYS
                        Always try to loan all added books. Can improve speed
                        if you're planning to download only books that need to
                        be loaned.
```
The `email` and `password` fields are required if the books you want to download need to be borrowed.
The `-r` argument specifies the resolution of the images (0 is the best quality).
The PDF are saved in the current folder

### Example
This command will download the 3 books as pdf in the best possible quality. To only downlaod the individual images you can use `--jpg`.
```sh
python3 archive-org-downloader.py -e myemail@tempmail.com -p Passw0rd -r 0 https://archive.org/details/IntermediatePython https://archive.org/details/horrorgamispooky0000bidd_m7r1 https://archive.org/details/elblabladelosge00gaut
```

This command will allow you to download books which do not need to be borrowed without owning an archive.org account:
```sh
python3 archive-org-downloader.py -s true -r 0 -u https://archive.org/details/IntermediatePython -u https://archive.org/details/jstor-4560629 -u https://archive.org/details/jstor-41455353
```

You can omit the `https://archive.org/details/` prefix and give book ids (without any `/`):
```sh
python3 archive-org-downloader.py -e myemail@tempmail.com -p Passw0rd -r 0 IntermediatePython horrorgamispooky0000bidd_m7r1 elblabladelosge00gaut
```

If you want to download a lot of books in a raw you can paste the urls of the books in a .txt file (one per line) and use `@file`
```sh
python3 archive-org-downloader.py -e myemail@tempmail.com -p Passw0rd @books_to_download.txt
```

You can also put your credentials (and any other arguments) in an `@file`:
```
-e
myemail@tempmail.com
-p
Passw0rd
```

Note: the previously required `-u` `--url` `-f` `--file` options have been removed.
Now just put the URLs without `-u`, and use `@myfile` instead of `-f myfile`.

## Donation

If you want to support MiniGlome's work, see <https://github.com/MiniGlome/Archive.org-Downloader#donation>.

If you want to support the Internet Archive, see <https://archive.org/donate>.
