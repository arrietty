#!/usr/bin/env python3
from typing import Tuple
import requests
import random, string
from concurrent import futures
from requests import Session
from tqdm import tqdm
import time
from datetime import datetime
import argparse
import os
import sys
import shutil
import json
from xdg.BaseDirectory import xdg_config_home

def display_error(response, message):
	print(message)
	print(response)
	print(response.text)
	exit()

def get_book_infos(session: Session, url: str) -> dict:
	r = session.get(url).text
	infos_url = "https:" + r.split('"url":"')[1].split('"')[0].replace("\\u0026", "&")
	response = session.get(infos_url)
	data = response.json()['data']
	return data

def format_data(content_type, fields):
	data = ""
	for name, value in fields.items():
		data += f"--{content_type}\x0d\x0aContent-Disposition: form-data; name=\"{name}\"\x0d\x0a\x0d\x0a{value}\x0d\x0a"
	data += content_type+"--"
	return data

def login(email: str, password: str) -> Session:
	print('Logging in...')
	session = requests.Session()
	session.get("https://archive.org/account/login")
	content_type = "----WebKitFormBoundary"+"".join(random.sample(string.ascii_letters + string.digits, 16))

	headers = {'Content-Type': 'multipart/form-data; boundary='+content_type}
	data = format_data(content_type, {"username":email, "password":password, "submit_by_js":"true"})

	response = session.post("https://archive.org/account/login", data=data, headers=headers)
	if "bad_login" in response.text:
		print("[-] Invalid credentials!")
		exit()
	elif "Successful login" in response.text:
		print("[+] Successful login")
		return session
	else:
		display_error(response, "[-] Error while login:")


def loan(session: Session, book_id: str, verbose: bool = True) -> Tuple[Session, bool]:
	print('Loaning book...')
	data = {
		"action": "grant_access",
		"identifier": book_id
	}
	# 2022-07-03: This request is done by the website but we don't need to do it here.
	# response = session.post("https://archive.org/services/loans/loan/searchInside.php", data=data)
	data['action'] = "browse_book"
	response = session.post("https://archive.org/services/loans/loan/", data=data)

	if response.status_code == 400 :
		if response.json()["error"] == "This book is not available to borrow at this time. Please try again later.":
			print("This book doesn't need to be borrowed")
			return session, False
		else :
			display_error(response, "Something went wrong when trying to borrow the book.")

	data['action'] = "create_token"
	response = session.post("https://archive.org/services/loans/loan/", data=data)

	if "token" in response.text:
		if verbose:
			print("[+] Successful loan")
		return session, True
	else:
		display_error(response, "Something went wrong when trying to borrow the book, maybe you can't borrow this book.")

def return_loan(session: Session, book_id: str):
	print('Returning book...')
	data = {
		"action": "return_loan",
		"identifier": book_id
	}
	response = session.post("https://archive.org/services/loans/loan/", data=data)
	if response.status_code == 200 and response.json()["success"]:
		print("[+] Book returned")
	else:
		display_error(response, "Something went wrong when trying to return the book")

def image_name(pages, page, directory):
	return f"{directory}/{(len(str(pages)) - len(str(page))) * '0'}{page}.jpg"

def download_one_image(session: Session, hasLoanedBook: bool, link: str, i, directory, book_id: str, pages):
	image = image_name(pages, i, directory)
	if not os.path.exists(image):
		headers = {
			"Referer": "https://archive.org/",
			"Accept": "image/avif,image/webp,image/apng,image/*,*/*;q=0.8",
			"Sec-Fetch-Site": "same-site",
			"Sec-Fetch-Mode": "no-cors",
			"Sec-Fetch-Dest": "image",
		}
		retry = True
		while retry:
			try:
				response = session.get(link, headers=headers)
				if response.status_code == 403:
					if hasLoanedBook:
						session, hasLoaned = loan(session, book_id, verbose=False)
						raise Exception("Borrow again")
					else:
						raise Exception("Error 403")
				elif response.status_code == 200:
					retry = False
			except KeyboardInterrupt:
				raise
			except:
				time.sleep(1)	# Wait 1 second before retrying

		tmpimage = image + ".tmp"
		with open(tmpimage, "wb") as f:
			f.write(response.content)
		os.rename(tmpimage, image)

def download(session: Session, hasLoanedBook: bool, n_threads, directory, links: list, scale: int, book_id: str):
	print("Downloading pages...")
	links = [f"{link}&rotate=0&scale={scale}" for link in links]
	pages = len(links)

	tasks = []
	with futures.ThreadPoolExecutor(max_workers=n_threads) as executor:
		try:
			for link in links:
				i = links.index(link)
				tasks.append(executor.submit(download_one_image, session=session, hasLoanedBook=hasLoanedBook, link=link, i=i, directory=directory, book_id=book_id, pages=pages))
			for task in tqdm(futures.as_completed(tasks), total=len(tasks)):
				task.result()
		except:
			executor.shutdown(wait=True, cancel_futures=True)
			raise

	images = [image_name(pages, i, directory) for i in range(len(links))]
	return images

def make_pdf(images, metadata, dir, book_id, directory):
	import img2pdf

	# prepare PDF metadata
	# sometimes archive metadata is missing
	pdfmeta = { }
	# ensure metadata are str
	for key in ["title", "creator", "associated-names"]:
		if key in metadata:
			if isinstance(metadata[key], str):
				pass
			elif isinstance(metadata[key], list):
				metadata[key] = "; ".join(metadata[key])
			else:
				raise Exception("unsupported metadata type")
	# title
	if 'title' in metadata:
		pdfmeta['title'] = metadata['title']
	# author
	if 'creator' in metadata and 'associated-names' in metadata:
		pdfmeta['author'] = metadata['creator'] + "; " + metadata['associated-names']
	elif 'creator' in metadata:
		pdfmeta['author'] = metadata['creator']
	elif 'associated-names' in metadata:
		pdfmeta['author'] = metadata['associated-names']
	# date
	if 'date' in metadata:
		try:
			pdfmeta['creationdate'] = datetime.strptime(metadata['date'][0:4], '%Y')
		except:
			pass
	# keywords
	pdfmeta['keywords'] = [f"https://archive.org/details/{book_id}"]

	pdf = img2pdf.convert(images, **pdfmeta)
	file = book_id + ".pdf"
	with open(os.path.join(dir, file),"wb") as f:
		f.write(pdf)
		print(f"[+] PDF saved as \"{file}\"")
		shutil.rmtree(directory)

def main():
	my_parser = argparse.ArgumentParser(fromfile_prefix_chars='@', epilog='You can use @myfile to read arguments from the file myfile, one per line.')
	my_parser.add_argument('-e', '--email', help='Your archive.org email', type=str, required=False)
	my_parser.add_argument('-p', '--password', help='Your archive.org password', type=str, required=False)
	my_parser.add_argument('-s', '--skip_login', help='Use this if you want to download books which do not need to be borrowed.', type=bool, default=False, required=False)
	my_parser.add_argument('-l', '--loan_always', help='Always try to loan all added books. Can improve speed if you\'re planning to download only books that need to be loaned.', type=bool, default=False, required=False)
	my_parser.add_argument('-d', '--dir', help='Output directory', type=str)
	my_parser.add_argument('-r', '--resolution', help='Image resolution (10 to 0, 0 is the highest), [default 3]', type=int, default=3)
	my_parser.add_argument('-t', '--threads', help="Maximum number of threads, [default 50]", type=int, default=50)
	my_parser.add_argument('-j', '--jpg', help="Output to individual JPG's rather than a PDF", action='store_true')
	my_parser.add_argument('-m', '--meta', help="Output the metadata of the book to a JSON file", action='store_true')
	my_parser.add_argument('URL', help='Link to the book (https://archive.org/details/XXXX). You can use this argument several times to download multiple books', type=str, nargs='+')

	argv = []
	conf = os.path.join(xdg_config_home, os.path.basename(sys.argv[0]) + ".conf")
	if os.path.exists(conf):
		print(f"configuration file \"{conf}\" found")
		argv.append('@' + conf)
	else:
		print(f"configuration file \"{conf}\" missing")

	for i in range(len(sys.argv)):
		if i > 0:
			argv.append(sys.argv[i])

	if len(sys.argv) == 1:
		my_parser.print_help(sys.stderr)
		sys.exit(1)
	args = my_parser.parse_args(argv)

	if args.URL is None or len(args.URL) < 1:
		my_parser.error("At least one url is required")

	email = args.email
	password = args.password
	skip_login = args.skip_login
	loginCredentialsAreAvailable = email is not None and password is not None

	if not skip_login and not loginCredentialsAreAvailable:
		print('Warning: Email/Password not given. Either provide email and password or use \'-s true\' to skip login if you only want to download books which do not need to be borrowed.')
		sys.exit(1)
	elif args.loan_always and not loginCredentialsAreAvailable:
		print('Warning: \'loan_always\' and \'skip_login\' parameters are both True while email/password is not given. Either provide email and password or remove \'loan_always\' parameter.')
		sys.exit(1)

	scale = args.resolution
	n_threads = args.threads
	d = args.dir

	if d == None:
		d = os.getcwd()
	elif not os.path.isdir(d):
		print(f"Output directory does not exist!")
		exit()

	# Check the urls format
	urls = args.URL
	books = []
	for url in urls:
		if url.startswith("https://archive.org/details/"):
			book_id = list(filter(None, url.split("/")))[3]
			books.append((book_id, url))
		elif len(url.split("/")) == 1:
			books.append((url, "https://archive.org/details/" + url))
		else:
			print(f"{url} --> Invalid book. URL must start with \"https://archive.org/details/\", or be a book id without any \"/\"")
			exit()

	print(f"Found {len(urls)} book(s) to download with resolution {scale}")
	isLoggedIN = None  # Use this just in case this will be changed in the future e.g. to login regardless of 'skip_login' whenever login credentials are available.
	if skip_login:
		session = Session()
		isLoggedIN = False
	else:
		session = login(email, password)
		isLoggedIN = True

	bookCounter = 0
	for book in books:
		bookCounter += 1
		book_id = book[0]
		url = book[1]
		print("="*40)
		print(f"Current book ({bookCounter}/{len(urls)}): https://archive.org/details/{book_id}")

		pdffile = os.path.join(d, book_id + ".pdf")
		if os.path.isfile(pdffile):
			print(f"Skipping download of existing file: {pdffile}")
			continue

		hasLoanedBook = False
		try:
			# loan book
			if args.loan_always:
				session, hasLoanedBook = loan(session, book_id)
				data = get_book_infos(session, url)
			else:
				# Check if book needs to be loaned and loan it
				hasAttemptedToLoanBook = False
				while True:
					data = get_book_infos(session, url)
					lendingInfo = data['lendingInfo']
					if hasAttemptedToLoanBook:
						break
					# Example book with no lending required: https://archive.org/details/IntermediatePython
					if lendingInfo['isLendingRequired']:
						if not isLoggedIN:
							# Example: https://archive.org/details/sim_interview_1998-11_28_11
							# We could download the preview pages of such books but this would kinda defeat the purose of this script
							print(f"[-] Cannot download this book without login credentials")
							exit()
						session, hasLoanedBook = loan(session, book_id)
						hasAttemptedToLoanBook = True
						continue
					else:
						hasLoanedBook = False
						break

			# get metadata
			metadata = data['metadata']
			links = []
			for item in data['brOptions']['data']:
				for page in item:
					links.append(page['uri'])
			if len(links) == 0:
				print(f"[-] Error while getting image links")
				raise Exception("Error while getting image links")
			print(f"[+] Found {len(links)} pages")

			directory = os.path.join(d, book_id)
			try:
				os.makedirs(directory)
			except FileExistsError:
				pass

			if args.meta:
				metafile = os.path.join(d, book_id + ".json")
				with open(metafile, 'w') as f:
					json.dump(metadata, f)
					print(f"[+] metadata saved as \"{metafile}\"")

			images = download(session, hasLoanedBook, n_threads, directory, links, scale, book_id)

			if not args.jpg: # Create pdf with images and remove the images folder
				make_pdf(images, metadata, args.dir if args.dir != None else "", book_id, directory)

		finally:
			if hasLoanedBook:
				return_loan(session, book_id)
	
if __name__ == "__main__":
	try:
		main()
	except KeyboardInterrupt:
		sys.exit(1)
	print("=" * 40)
	print('All done!')
