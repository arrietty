DESTDIR = $(HOME)
prefix = .local
BINDIR = $(DESTDIR)/$(prefix)/bin
MANDIR = $(DESTDIR)/$(prefix)/share/man/man1

all: arrietty arrietty.1.gz

arrietty: archive-org-downloader.py
	dos2unix -n archive-org-downloader.py arrietty
	chmod a+x arrietty

arrietty.1.gz: arrietty.1.md
	pandoc arrietty.1.md --standalone -o arrietty.1
	gzip -9 -f arrietty.1

install: arrietty arrietty.1.gz
	install -d $(MANDIR)
	install -m 644 arrietty.1.gz $(MANDIR)
	install -d $(BINDIR)
	install -m 755 arrietty $(BINDIR)/arrietty
	mandb

.PHONY: all install
.SUFFIXES:
